# developed with Python 3.5

## Script to obtain main data from page listing standings in a mod-5 round
import os
import time
import sys
import socket
import sqlite3 as lite
import bs4 as bs
import urllib.request

from w3lib.url import safe_url_string

start_time = time.time()

# timeout in seconds
timeout = 220
socket.setdefaulttimeout(timeout)

# path
path = '/home/c/Projects/master/goalsurvival'

# set working directory
os.chdir(path)

class Standings:
    def __init__(self):
        self.links = [
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/IqtSx2Jq/ts/UaIJ6b2H/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/QX85rfUD/ts/WzgYhIor/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/QHl9iR3f/ts/nugON26l/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/tzt9XfmD/ts/pr9mXmnf/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/x2gVCNwJ/ts/Sxm3klBf/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/b7o7GF2h/ts/lCDqolol/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/8WfvLzqU/ts/0M9LMq9C/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/KQ8NtFnh/ts/EuKuM5MI/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/plbWD5hI/ts/IclHToOB/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/SGarA0rG/ts/rH7SFawI/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/nNYg5pkp/ts/GMo9gNYh/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/UV4Vlz26/ts/2ZeoF4uk/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/M1VFOdWr/ts/pYi5cMuA/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/Qyo0OIgA/ts/zcgMPDzF/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/xQV0Y9j3/ts/ljIBgFCg/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/t88zoFDC/ts/2wDJRkib/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/OtIGJDpL/ts/Uc3Ascea/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/hK4hu76a/ts/U9wG88N6/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/OhnzLqf7/ts/faBBhyuM/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/8Ai8InSt/ts/fZHsKRg9/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/hUnbv6tG/ts/db6x80Ib/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/EmMn2XCi/ts/G0rc7gAt/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/jLZ9xBGT/ts/EDRix5yC/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/2Ll8xq90/ts/OIZw1WG1/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/OO2KUIR8/ts/pSvKVQK1/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/EubWlaac/ts/pvidjVHM/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/MeC5veFK/ts/0hQKemI3/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/A1AAw29q/ts/p4jMxzfU/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/xQgW7bOL/ts/tU4tmZrf/',
            'http://www.oddsportal.com/ajax-standings/tab/form/table_sub//t/lvMwpKPH/ts/vcYBi9B8/']

    def get_teams(self, link):
        string = link
        url = safe_url_string(string)
        req = urllib.request.Request(url)
        source = urllib.request.urlopen(req, timeout = timeout)
        soup = bs.BeautifulSoup(source, 'lxml')
        #retrieve teams
        array = soup.find_all("span", class_ = "team_name_span")
        equipes = []
        for arr in array:
            equipes.append(arr.get_text())
        tam = int(len(equipes)/6)
        teams = []
        for i in range(1,7):
            ronda = []
            ronda = equipes[(tam*(i-1)):(tam*i)]
            teams.append(ronda)
        # print(len(equipes), tam, len(equipes)/tam)
        # print(len(teams))
        return teams

    def build_sql(self, *args):
        string = ''
        inter = ','
        # equipes = args[0], league = array[1], group = array[2]
        p = 1
        teams_sql = []
        for item in args[0]:
            equipe = '\'' + str(item) + '\''
            string = str(args[1]) + inter + str(args[2]) + inter + equipe + inter + str(p)
            p += 1
            teams_sql.append(string)
        #
        return teams_sql

    def db_sql(self, *teams_sql):
        #insert array of values
        sql = 'INSERT INTO standings(league_season, rounds, team, position) VALUES ('
        con = lite.connect('foot.db')
            #go through array elements
        for item in teams_sql:

            with con:
                cur = con.cursor()
                string = sql + item + ')'
                # print(string)
                cur.execute(string)
        if con:
            con.close()

    def sql_seq(self, *args):
        teams_sql = []
        league = args[1]
        teams = args[0]
        group = 1
        for ronda in teams:
            args = [ronda, league, group*5]
            teams_sql = self.build_sql(*args)
            self.db_sql(*teams_sql)
            group += 1

    def lookup(self, links):
        league = 1
        for link in links:
            teams = self.get_teams(link)
            args = [teams, league]
            self.sql_seq(*args)
            league += 1

    def run(self):
        links = self.links
        self.lookup(links)

S = Standings()
S.run()
#EOF
