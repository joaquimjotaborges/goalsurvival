# -*- coding: iso-8859-15 -*-
# developed with Python 3.5
# previously:
# pip3 install BeautifulSoup4
## Script to obtain data about matches as they are included in the table populated before
import time, sys, socket
import sqlite3 as lite
import bs4 as bs
import urllib.request

from w3lib.url import safe_url_string

start_time = time.time()

# timeout in seconds
timeout = 90
socket.setdefaulttimeout(timeout)
#connect to db
con = lite.connect('foot.db')
#production lookup match table
matches = []
with con:
    cur = con.cursor()
    #retrieve league_season table rows
    cur.execute("SELECT id, link FROM match WHERE id > 507")
    rows = cur.fetchall()
    matches = rows
if con:
    con.close()
#
jogos = 1
for game in matches:
    start_time2 = time.time()
    if game[1] == "no link":
        con = lite.connect('foot.db')
        cur = con.cursor()
        update = str(game[0]) + ', 0' + str(event_type[0]) + ', 1 ,90'
        sql = 'INSERT INTO event (match_id, event_type_id, team_type, tempo) VALUES ('+update+')'
        cur.execute(sql)
        update = str(game[0]) + ', 0' + str(event_type[0]) + ', 2 ,90'
        sql = 'INSERT INTO event (match_id, event_type_id, team_type, tempo) VALUES ('+update+')'
        cur.execute(sql)
        tempo2 = time.time() - start_time2
        print(jogo, 'inserted in: ',"%.2f" % tempo2, 'seconds')
    else:
        url = safe_url_string(game[1])
        req = urllib.request.Request(url)
        source = urllib.request.urlopen(req, timeout = timeout)
        soup = bs.BeautifulSoup(source, 'lxml')
        #retriev goal events
        golo = soup.find_all('span', class_ = 'slball')
        jogo = str(game[0])
    #
        # identify types of events => 0: goalless, 1: goal, 2: yellow card, 3: red card or second yellow card
        event_type = [0, 1, 2, 3]
        #and types of teams => 1: home, 2: away
        team_type = [1, 2]
        #retrieve card events
        yellow = soup.find_all('span', class_ = 'yellowcard')
        second_yellow = soup.find_all('span', class_ = 'second_yellowcard')
        redcard = soup.find_all('span', class_ = 'redcard')
        #declare variables
        gol_pais = []
        gol_pais_attrs = []
    #
        golos_home =  []
        golos_away = []
        yellow_pais = []
        yellow_home = []
        yellow_away =  []
        second_yellow_pais = []
        second_yellow_home = []
        second_yellow_away =  []
        red_pais = []
        red_home = []
        red_away = []
        red_home_pais = []
        red_away_pais = []
    #
        for gol in golo:
            gol_pais.append(gol.parent)
    #
        for attr in gol_pais:
            if attr.attrs['class'] == ['home']:
                golos_home.append(attr.find('span','slball').previous_element)
            if attr.attrs['class'] == ['away']:
                golos_away.append(attr.find('span','slball').next_element)
    #
        for y in yellow:
            yellow_pais.append(y.parent)
    #
        for attr in yellow_pais:
            if attr.attrs['class'] == ['home']:
                yellow_home.append(attr.find('span','yellowcard').previous_element)
            if attr.attrs['class'] == ['away']:
                yellow_away.append(attr.find('span','yellowcard').next_element)
    #
        for sy in second_yellow:
            second_yellow_pais.append(sy.parent)
    #
        for attr in second_yellow_pais:
            if attr.attrs['class'] == ['home']:
                red_home.append(attr.find('span','second_yellowcard').previous_element)
            if attr.attrs['class'] == ['away']:
                red_away.append(attr.find('span','second_yellowcard').next_element)
    #
        for r in redcard:
            red_pais.append(r.parent)
    #
        for attr in red_pais:
            if attr.attrs['class'] == ['home']:
                red_home.append(attr.find('span','redcard').previous_element)
            if attr.attrs['class'] == ['away']:
                red_away.append(attr.find('span','redcard').next_element)
    #
        con = lite.connect('foot.db')
        with con:
            cur = con.cursor()
            if len(golos_home) == 0:
                update = jogo + ', 0, 1, 90'
                sql = 'INSERT INTO event (match_id, event_type_id, team_type, tempo) VALUES ('+update+')'
                cur.execute(sql)
                #print(sql)
            else:
                for i in range(0, len(golos_home)):
                    update =  ''
                    arr = []
                    num = golos_home[i].replace('\'','').rstrip(' ')
                    if (num.find('+') != -1):
                        arr = num.split('+')
                        if (int(arr[0])==45):
                            golos_home[i] = int(arr[0])
                        else:
                            golos_home[i] = int(arr[0])+int(arr[1])
                    else:
                        golos_home[i] = int(num.rstrip('\' '))
                    update = jogo + ',' + str(event_type[1]) + ',' + str(team_type[0]) + ',' + str(golos_home[i])
                    sql =  'INSERT INTO event(match_id,event_type_id,team_type,tempo) VALUES ('+update+')'
                    cur.execute(sql)
                    #print(sql)
        if con:
            con.close()
    #
        con = lite.connect('foot.db')
        with con:
            cur = con.cursor()
            if len(golos_away) == 0:
                update = jogo + ', 0, 2, 90'
                sql = 'INSERT INTO event (match_id, event_type_id, team_type, tempo) VALUES ('+update+')'
                cur.execute(sql)
                #print(sql)
            else:
                for i in range(0, len(golos_away)):
                    update = ''
                    arr = []
                    num = golos_away[i].replace('\'','').lstrip(' ')
                    if (num.find('+') != -1):
                        arr = num.split('+')
                        if (int(arr[0])==45):
                            golos_away[i] = int(arr[0])
                        else:
                            golos_away[i] = int(arr[0])+int(arr[1])
                    else:
                        golos_away[i] = int(golos_away[i].rstrip('\' '))
                    update = jogo + ',' + str(event_type[1]) + ',' + str(team_type[1]) + ',' + str(golos_away[i])
                    sql =  'INSERT INTO event(match_id,event_type_id,team_type,tempo) VALUES (' + update + ')'
                    cur.execute(sql)
                    #print(sql)
        if con:
            con.close()
    #
        con = lite.connect('foot.db')
        with con:
            cur = con.cursor()
            for i in range(0, len(yellow_home)):
                update = ''
                arr = []
                num = yellow_home[i].replace('\'','').rstrip(' ')
                if (num.find('+') != -1):
                    arr = num.split('+')
                    if (int(arr[0])==45):
                        yellow_home[i] = int(arr[0])
                    else:
                        yellow_home[i] = int(arr[0])+int(arr[1])
                else:
                    yellow_home[i] = int(num.rstrip('\' '))
                update = jogo+','+str(event_type[2])+','+str(team_type[0])+','+str(yellow_home[i])
                sql =  'INSERT INTO event(match_id,event_type_id,team_type,tempo) VALUES ('+update+')'
                cur.execute(sql)
                #print(sql)
        if con:
            con.close()
    #
        con = lite.connect('foot.db')
        with con:
            cur = con.cursor()
            for i in range(0, len(yellow_away)):
                update = ''
                arr = []
                num = yellow_away[i].replace('\'','').lstrip(' ')
                if (num.find('+') != -1):
                    arr = num.split('+')
                    if (int(arr[0])==45):
                        yellow_away[i] = int(arr[0])
                    else:
                        yellow_away[i] = int(arr[0])+int(arr[1])
                else:
                    yellow_away[i] = int(yellow_away[i].rstrip('\' '))
                update = jogo+','+str(event_type[2])+','+str(team_type[1])+','+str(yellow_away[i])
                sql =  'INSERT INTO event(match_id,event_type_id,team_type,tempo) VALUES ('+update+')'
                cur.execute(sql)
                #print(sql)
        if con:
            con.close()

        if (len(red_home)>0):
            con = lite.connect('foot.db')
            with con:
                cur = con.cursor()
                for i in range(0,len(red_home)):
                    update = ''
                    arr = []
                    num = red_home[i].replace('\'','').rstrip(' ')
                    if (num.find('+') != -1):
                        arr = num.split('+')
                        if (int(arr[0])==45):
                            red_home[i] = int(arr[0])
                        else:
                            red_home[i] = int(arr[0])+int(arr[1])
                    else:
                        red_home[i] = int(num.rstrip('\' '))
                    update = jogo+','+str(event_type[3])+','+str(team_type[0])+','+str(red_home[i])
                    sql = 'INSERT INTO event(match_id,event_type_id,team_type,tempo) VALUES ('+update+')'
                    cur.execute(sql)
                    #print(sql)
            if con:
                con.close()

        if (len(red_away)>0):
            con = lite.connect('foot.db')
            with con:
                cur = con.cursor()
                for i in range(0,len(red_away)):
                    update = ''
                    arr = []
                    num = red_away[i].replace('\'','').lstrip(' ')
                    if (num.find('+') != -1):
                        arr = num.split('+')
                        if (int(arr[0])==45):
                            red_away[i] = int(arr[0])
                        else:
                            red_away[i] = int(arr[0])+int(arr[1])
                    else:
                        red_away[i] = int(red_away[i].rstrip('\' '))
                    update = jogo+','+str(event_type[3])+','+str(team_type[1])+','+str(red_away[i])
                    sql = 'INSERT INTO event(match_id,event_type_id,team_type,tempo) VALUES ('+update+')'
                    cur.execute(sql)
                    #print(sql)
            if con:
                con.close()
        tempo2 = time.time() - start_time2
        t = time.time() - start_time
        print('match no. ', jogo, 'inserted in: ',"%.2f" % tempo2, 'seconds, time from the start: ',"%.2f" % t, " seconds")
        jogos = jogos + 1

tempo = time.time() - start_time
print(jogos-1, " matches inserted in a total time of: ","%.2f" % tempo, 'seconds')

#EOF
