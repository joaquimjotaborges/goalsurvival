# developed with Python 3.5

# previously:
# pip3 install BeautifulSoup4
# pip3 install w3lib

## Script to create the database and the planned tables
## this also populates the league_season table

#import modules
import time
start_time = time.time()

import sys
import sqlite3 as lite

#create and close database file
with open('foot.db', 'w+') as db:
    db.read()
    del db

#connect to db
con = lite.connect('foot.db')
with con:
    cur = con.cursor()
    #create tables
    cur.execute("CREATE TABLE IF NOT EXISTS 'event' ('id' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , 'match_id' INTEGER NOT NULL  ,'event_type_id' INTEGER NOT NULL  , 'team_type' INTEGER NOT NULL , 'tempo' INTEGER NOT NULL)")
    cur.execute("CREATE TABLE IF NOT EXISTS 'league_season' ('league' INTEGER NOT NULL  UNIQUE , 'season' VARCHAR NOT NULL , 'rounds' INTEGER NOT NULL )")
    cur.execute("CREATE TABLE IF NOT EXISTS 'match' ('id' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , 'home' VARCHAR NOT NULL , 'away' VARCHAR NOT NULL , 'final_score_home' INTEGER NOT NULL , 'final_score_away' INTEGER NOT NULL , 'league_season' INTEGER NOT NULL , 'rounds' INTEGER NOT NULL , 'link' VARCHAR NOT NULL)")
    cur.execute("CREATE TABLE IF NOT EXISTS 'teams' ('id' INTEGER PRIMARY KEY NOT NULL UNIQUE , 'name' VARCHAR NOT NULL  UNIQUE )")
#    cur.execute("CREATE TABLE 'standings' ('id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, 'league_season' INTEGER NOT NULL, 'rounds' INTEGER NOT NULL, 'team' TEXT NOT NULL, 'position' INTEGER NOT NULL)")
if con:
    con.close()

#populate league_season table
ligas = [(1,"portugal/primeira-liga/2012-2013",30),(2,"portugal/primeira-liga/2013-2014",30),(3,"portugal/primeira-liga/2014-2015",34),(4,"portugal/primeira-liga/2015-2016",34),(5,"portugal/primeira-liga/2016-2017",34),(6,"spain/primera-division/2012-2013",38),(7,"spain/primera-division/2013-2014",38),(8,"spain/primera-division/2014-2015",38),(9,"spain/primera-division/2015-2016",38),(10,"spain/primera-division/2016-2017",38),(11,"germany/bundesliga/2012-2013",34),(12,"germany/bundesliga/2013-2014",34),(13,"germany/bundesliga/2014-2015",34),(14,"germany/bundesliga/2015-2016",34),(15,"germany/bundesliga/2016-2017",34),(16,"england/premier-league/2012-2013",38),(17,"england/premier-league/2013-2014",38),(18,"england/premier-league/2014-2015",38),(19,"england/premier-league/2015-2016",38),(20,"england/premier-league/2016-2017",38),(21,"france/ligue-1/2012-2013",38),(22,"france/ligue-1/2013-2014",38),(23,"france/ligue-1/2014-2015",38),(24,"france/ligue-1/2015-2016",38),(25,"france/ligue-1/2016-2017",38),(26,"italy/serie-a/2012-2013",38),(27,"italy/serie-a/2013-2014",38),(28,"italy/serie-a/2014-2015",38),(29,"italy/serie-a/2015-2016",38),(30,"italy/serie-a/2016-2017",38)]

con = lite.connect('foot.db')

for liga in ligas:
    with con:
        cur = con.cursor()
        lig = str(liga[0]) + ',' + '\'' + str(liga[1]) + '\'' + ',' + str(liga[2])
        sql = "INSERT INTO league_season(league,season,rounds) VALUES ("+lig+")"
        # print(sql)
        cur.execute(sql)

if con:
    con.close()

tempo = time.time() - start_time
print("DB foot.db and table league_season created and populated in => ", "%.2f" % tempo, 'seconds')
