# developed with Python 3.5

# previously:
# pip3 install BeautifulSoup4
# pip3 install w3lib

## Script to obtain main data from page listing a round of the included leagues
import time
import sys
import socket
import sqlite3 as lite
import bs4 as bs
import urllib.request

from w3lib.url import safe_url_string

start_time = time.time()

# timeout in seconds
timeout = 220
socket.setdefaulttimeout(timeout)
#connect to db
con = lite.connect('foot.db')
#ligas
ligas = []
with con:
    cur = con.cursor()
    #retrieve league_season table rows
    cur.execute("SELECT * FROM league_season")
    rows = cur.fetchall()
    ligas = rows
if con:
    con.close()
#production
for liga in ligas:
    start_time1 = time.time()
    #prefix of the link that identifies the round page
    jornada = 'http://www.scorespro.com/soccer/' + liga[1] + '/round-'
    #e.g: jornada = 'http://www.scorespro.com/soccer/portugal/primeira-liga/2014-2015/round-6/'
    for ronda in range(1, liga[2]+1):
        start_time2 = time.time()
        #list of data of comma separated values to write to db
        lista = []
        url = safe_url_string(jornada + str(ronda))
        req = urllib.request.Request(url)
        source = urllib.request.urlopen(req, timeout=timeout)
        #BeautifulSoup object
        soup = bs.BeautifulSoup(source, 'lxml')
        # anchors with the same class of score_link that points to match page
        scores = soup.find_all('td', class_='score')
        # array to put links
        links = []
#
        for score in scores:
            if not score.a:
                links.append('\'no link\'')
            else:
                string = score.a['href']
                # strip the string from js code
                link = string.strip('javascript:pop(')
                # get position of the comma and the length
                num = link.find(',')
                length = len(link)
                # get the last part of the link
                link = '\'http://www.scorespro.com/soccer/livescore/'+link[num+2:length-2]+'/\''
                # append link to the array of links
                links.append(link)
#
        # find home and away teams
        # 1st: obtain country name
        country = liga[1][0:liga[1].find('/')]
        teams = soup.select('a[href^="/soccer/'+country+'/teams"]')
        # declare vectors for home teams and away teams
        home_teams = []
        away_teams = []
        #iterative variable
        j=0
        #obtain home and away teams
        for team in teams:
            if (j%2==0):
                home_teams.append(team.get_text())
            if (j%2!=0):
                away_teams.append(team.get_text())
            j=j+1
        #zero iterative variable
        j=0
#
        #obtain the final score
        final_score = soup.find_all('td', class_='score')
        # declare vectors for scorer of home team and away team
        final_home = []
        final_away = []
        #obtain final score
        for score in final_score:
            home = score.get_text()
            homey = home[0:home.find('-')].rstrip(' ')
            final_home.append(homey)
            away = home[home.find('-'):len(home)].lstrip('- ')
            final_away.append(away)
#
        for item in range(0, len(scores)):
            #include data to be stored in table
            frase = '\''+home_teams[item]+'\','
            frase = frase + '\''+away_teams[item]+'\','
            frase = frase + final_home[item]+','
            frase = frase + final_away[item]+','
            frase = frase + str(liga[0])+','
            frase = frase + str(ronda)+','
            frase = frase + links[item]
            # append data to list
            lista.append(frase)
        con = lite.connect('foot.db')
        for li in lista:
            sql = 'INSERT INTO match(home,away,final_score_home,final_score_away,league_season,rounds,link) VALUES ('+li+');'
            with con:
                cur = con.cursor()
                #insert data
                cur.execute(sql)
                # print('. ', end=""),
        if con:
            con.close()
        tempo2 = time.time() - start_time2
        print("inserida jornada ", ronda, liga[1], " => ", "%.2f" % tempo2, 'segundos')
    tempo = time.time() - start_time1
    print("\ninserida liga ", liga[1], " => ", "%.2f" % tempo, 'segundos\n\n')
#
tempo = time.time() - start_time
print("\nrotina executada => ","%.2f" % tempo, 'segundos')
#
#EOF
